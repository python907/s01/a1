# Create 5 variables:

name = "April"
age = 20
occupation = "Engineer"
movie = "Avatar"
rating = 90.5

print(f"I am {name}, and I am {age} years old, I work as an {occupation}, and my rating for {movie} is {rating}%")

# Create 3 variables

num1, num2, num3 = 36, 4, 21
print(num1 * num2) # 144
print(num1 < num3) # False

num2 += num3
print(num2) # 25
